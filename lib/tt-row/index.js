import TtRow from './src/mm-row.vue'

TtRow.install = function (Vue) {
  Vue.component(TtRow.name, TtRow)
}

export default TtRow
