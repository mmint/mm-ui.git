import TtImage from './src/mm-image.vue'

TtImage.install = function (Vue) {
  Vue.component(TtImage.name, TtImage)
}

export default TtImage
