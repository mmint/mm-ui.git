// import Vue from "vue";
import Engine from "./engine/engineLoader";
export * from "./bean";
export * from "./beanType";

import Button from './mm-button';
import Column from './mm-column';
import Dialog from './mm-dialog';
import Image from './mm-image';
import ImageViewer from './mm-image-viewer';
import Input from './mm-input';
import Menu from './mm-menu';
import Mind from './mm-mind';
import Progress from './mm-progress';
import Row from './mm-row';
import Select from './mm-select';
import SelectCheck from './mm-select-check';
import SelectRange from './mm-select-range';
import SelectSwitch from './mm-select-switch';
import Tab from './mm-tab';
import Table from './mm-table';
import TableItem from './mm-table-item';
import TablePage from './mm-table-page';
import Text from './mm-text';
import component1 from './component1';


const components = [
  Button, Column, Dialog, Image,
  ImageViewer, Input, Menu, Mind,
  Progress, Row, Select, SelectCheck,
  SelectRange, SelectSwitch, Tab, Table,
  TableItem, TablePage, Text, component1
]

function install(Vue) {
  components.map(component => {
    Vue.component(component.name, component)
  })
}
// export {
//   install,
//   component1,
//    Button,
//    Column
// }
export default {
  api: Engine(),
  install,
  Button,
  Column,
  Dialog,
  Image,
  ImageViewer,
  Input,
  Menu,
  Mind,
  Progress,
  Row,
  Select,
  SelectCheck,
  SelectRange,
  SelectSwitch,
  Tab,
  Table,
  TableItem,
  TablePage,
  Text,
  component1
}


// vue、element-ui、axios、js-base64、xlsx


// export default {
//   install() {
//     Vue.component("mm-input", () => import("./components/mm-input.vue")); // 输入框
//     Vue.component("mm-select", () => import("./components/mm-select.vue")); // 选择框
//     Vue.component("mm-select-range", () => import("./components/mm-select-range.vue")); // 范围选择框
//     Vue.component("mm-select-check", () => import("./components/mm-select-check.vue")); // 勾选框
//     Vue.component("mm-select-switch", () => import("./components/mm-select-switch.vue")); // 滑块
//     Vue.component("mm-text", () => import("./components/mm-text.vue")); // 文本
//     Vue.component("mm-button", () => import("./components/mm-button.vue")); // 按钮
//     Vue.component("mm-image", () => import("./components/mm-image.vue")); // 图片
//     Vue.component("mm-progress", () => import("./components/mm-progress.vue")); // 进度条
//     Vue.component("mm-column", () => import("./components/mm-column.vue")); // 纵向布局
//     Vue.component("mm-row", () => import("./components/mm-row.vue")); // 横向布局
//     Vue.component("mm-menu", () => import("./components/mm-menu.vue")); // 导航菜单
//     Vue.component("mm-tab", () => import("./components/mm-tab.vue")); // 导航tab
//     Vue.component("mm-table", () => import("./components/mm-table.vue")); // 表格
//     Vue.component("mm-table-item", () => import("./components/mm-tableItem.vue")); // 表格的列
//     Vue.component("mm-table-page", () => import("./components/mm-tablePage.vue")); // 表格的页码
//     Vue.component("mm-dialog", () => import("./components/mm-dialog.vue")); // 对话框
//     Vue.component("mm-mind", () => import("./components/mm-mind.vue")); // 流程图
//   }

// }


// }