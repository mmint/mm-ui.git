import TtSelectRange from './src/mm-select-range.vue'

TtSelectRange.install = function (Vue) {
  Vue.component(TtSelectRange.name, TtSelectRange)
}

export default TtSelectRange
