import TtText from './src/mm-text.vue'

TtText.install = function (Vue) {
  Vue.component(TtText.name, TtText)
}

export default TtText
