export declare function Callback(success: boolean, data: any): void;
export declare function AnyCallback(data: any): void;
export declare function AnyCallback1(data: any,success:any): void;
export declare function AccountCallback(result: IAccount | null): void;


export interface IAccount {
  code: string;
  token: string;
}

export enum CheckType {
  text = "text",// 文本
  integer = "integer",// 正整数
  number = "number",// 正数
  integerAll = "integerAll",// 整数
  numberAll = "numberAll",// 数字
  phone = "phone",// 电话
  carNumber = "carNumber",// 车牌
  idCard = "idCard",// 身份证
  email = "email",// 邮箱
  http = "http",// 网址
  https = "https"// 网址
}

export interface IModal {
  title?: string;
  content?: string;
  showCancel?: boolean;// 不传则为true
  cancelText?: string;// 不传则为取消
  confirmText?: string;// 不传则为确定
}

// 输入组件、选择组件、对话框组件的数据操作对象
export interface IInputObj {
  // constructor(obj?: any);// obj不为空则添加obj所有属性

  setData(obj: any): void;// 添加obj所有属性到当前对象中

  getData(): object;// 获取所有已添加的属性的对象

  clearData(): void;// 移除所有已添加的属性

  checkData(key?: string): boolean;// 检查已添加属性的数据是否合法，不为空则检查对应属性的数据，为空则检查所有属性的数据

  setDialogVisible(visible: boolean): void;// 设置对话框是否可见

  isDialogVisible(): boolean;// 获取对话框显示状态
}

// 异步设置和获取的操作对象
export declare function AsynCallback(data: object | null): void;
export interface IAsynHandler {
  register(key: string): typeof Callback;
  getData(callback: typeof AsynCallback): void;
}

export interface IEngineLogic {
  checkEngine(isReset: boolean): void;
  clearLogicCache(): void;
  handleRequestOverdue(): void;
  handleRequestLogin(callback: typeof AccountCallback): void;
  getRequestHeader(linkId: string): object;
  getUploadFileHeader(): object;
}