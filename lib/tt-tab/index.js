import TtTab from './src/mm-tab.vue'

TtTab.install = function (Vue) {
  Vue.component(TtTab.name, TtTab)
}

export default TtTab
