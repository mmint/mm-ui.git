import TtTable from './src/mm-table.vue'

TtTable.install = function (Vue) {
  Vue.component(TtTable.name, TtTable)
}

export default TtTable
