import TtButton from './src/mm-button.vue'

TtButton.install = function (Vue) {
  Vue.component(TtButton.name, TtButton)
}

export default TtButton
