import TtSelectCheck from './src/mm-select-check.vue'

TtSelectCheck.install = function (Vue) {
  Vue.component(TtSelectCheck.name, TtSelectCheck)
}

export default TtSelectCheck
