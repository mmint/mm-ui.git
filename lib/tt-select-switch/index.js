import TtSelectSwitch from './src/mm-select-switch.vue'

TtSelectSwitch.install = function (Vue) {
  Vue.component(TtSelectSwitch.name, TtSelectSwitch)
}

export default TtSelectSwitch
