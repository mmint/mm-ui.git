import TtProgress from './src/mm-progress.vue'

TtProgress.install = function (Vue) {
  Vue.component(TtProgress.name, TtProgress)
}

export default TtProgress
