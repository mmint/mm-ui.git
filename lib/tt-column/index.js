import TtColumn from './src/mm-column.vue'

TtColumn.install = function (Vue) {
  Vue.component(TtColumn.name, TtColumn)
}

export default TtColumn
