import TtTablePage from './src/mm-table-page.vue'

TtTablePage.install = function (Vue) {
  Vue.component(TtTablePage.name, TtTablePage)
}

export default TtTablePage
