import TtSelect from './src/mm-select.vue'

TtSelect.install = function (Vue) {
  Vue.component(TtSelect.name, TtSelect)
}

export default TtSelect
