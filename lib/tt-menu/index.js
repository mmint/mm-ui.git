import TtMenu from './src/mm-menu.vue'

TtMenu.install = function (Vue) {
  Vue.component(TtMenu.name, TtMenu)
}

export default TtMenu
