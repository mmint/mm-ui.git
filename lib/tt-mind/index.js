import TtMind from './src/mm-mind.vue'

TtMind.install = function (Vue) {
  Vue.component(TtMind.name, TtMind)
}

export default TtMind
