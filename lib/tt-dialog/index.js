import TtDialog from './src/mm-dialog.vue'

TtDialog.install = function (Vue) {
  Vue.component(TtDialog.name, TtDialog)
}

export default TtDialog
