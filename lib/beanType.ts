/**
 * 获取定位是否成功
 * 成功则返回 type>=0 返回result.latitude、result.longitude
 * 成功则返回 type>=1 返回result.province、result.city、result.district
 * 成功则返回 type==2 返回result.address
 * 失败则返回 string类型的错误信息
 */
export interface ILocation {
  type: number; // 0只取经纬度，1取经纬度和省市区，2取经纬度、省市区和详细地址，不传则type=1
}

/**
 * 表格页码组件的数据操作对象
 */
export interface ITablePageObj {
  // constructor(page?: number, pageSize?: number, pageTotal?: number);

  page: number;// 不传则为1
  pageSize: number;// 不传则为10
  pageTotal: number;// 不传则为0
}