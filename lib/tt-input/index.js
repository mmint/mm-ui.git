import TtInput from './src/mm-input.vue'

TtInput.install = function (Vue) {
  Vue.component(TtInput.name, TtInput)
}

export default TtInput
