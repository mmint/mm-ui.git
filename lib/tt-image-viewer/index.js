import TtImageViewer from './src/mm-image-viewer.vue'

TtImageViewer.install = function (Vue) {
  Vue.component(TtImageViewer.name, TtImageViewer)
}

export default TtImageViewer
