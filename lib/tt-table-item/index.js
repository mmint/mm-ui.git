import TtTableItem from './src/mm-table-item.vue'

TtTableItem.install = function (Vue) {
  Vue.component(TtTableItem.name, TtTableItem)
}

export default TtTableItem
